package online.loveplay.service.bean;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Author ：zx
 * @Date ：14:47 2022/5/10
 * @Description ：标签集合
 */
@Data
public class BlogTagMap {

    private Map<String, BlogTag> map;

    public BlogTag get(String code){
        return this.map.get(code);
    }
    public String getName(String code){
        return this.map.get(code).getTagCode();
    }
    public void del(String code){
        this.map.remove(code);
    }
    public void clear(){
        this.map.clear();
    }
    public void add(BlogTag tag){
        this.map.put(tag.getTagCode(), tag);
    }
    public void addAll(List<BlogTag> list){
        for(BlogTag tag : list){
            this.map.put(tag.getTagCode(), tag);
        }
    }
}
