package online.loveplay.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2019-12-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MusicVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 音乐名称
     */
    private String name;

    /**
     * 作者
     */
    private String artist;

    /**
     * 歌曲来源
     */
    private String url;

    /**
     * 歌曲封面
     */
    private String cover;

    /**
     * 歌词
     */
    private String lrc;


}
