package online.loveplay.service.apiService;

import cn.hutool.core.bean.BeanUtil;
import online.loveplay.api.BeanUtils;
import online.loveplay.api.BusinessException;
import online.loveplay.api.dto.ArticleDto;
import online.loveplay.api.dto.BlogTagDto;
import online.loveplay.api.dto.TimeLineDto;
import online.loveplay.api.service.ArticleService;
import online.loveplay.api.vo.ArticleVo;

import online.loveplay.service.bean.Article;
import online.loveplay.service.bean.vo.TimeLineVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zx
 */
@DubboService
public class ArticleApiServiceImpl implements ArticleService{


    @Autowired
    private online.loveplay.service.service.ArticleService articleService;

    @Override
    public void updateArticle(ArticleVo articleVo) throws BusinessException {
        Article article = new Article ();
        BeanUtil.copyProperties(articleVo, article);
        articleService.updateArticle(article);
    }

    @Override
    public List<ArticleDto> getArticleByPage(ArticleVo articleVo) throws BusinessException {
        Article article = new Article ();
        BeanUtil.copyProperties(articleVo, article);

        List<Article> articleList = articleService.getArticleByPage(article);


        return BeanUtils.copy(articleList, ArticleDto.class);
    }

    @Override
    public ArticleDto searchById(int id) {
        Article a = articleService.searchById(id);
        ArticleDto articleDto = new ArticleDto();
        BeanUtil.copyProperties(a, articleDto);
        return articleDto;
    }

    @Override
    public List<ArticleDto> getArticleList(String keywords, Integer currentPage, Integer pageSize) {

        List<Article> articleList = articleService.getArticleList(keywords, currentPage, pageSize);

        return BeanUtils.copy(articleList, ArticleDto.class);
    }

    @Override
    public List<BlogTagDto> searchTag() {
        return BeanUtils.copy(articleService.searchTag(), BlogTagDto.class);
    }

    @Override
    public void deleteArticle(List<Integer> ids) throws BusinessException {
         articleService.deleteArticle(ids);
    }

    @Override
    public List<TimeLineDto> searchTimeLine() {
        List<TimeLineVo> timeLineVoList = articleService.searchTimeLine();

        return BeanUtils.copy(timeLineVoList, TimeLineDto.class);
    }

    @Override
    public ArticleDto searchByArticleIdAndManagerId(int articleId, int managerId) {
        Article article = articleService.searchByArticleIdAndManagerId(articleId, managerId);
        ArticleDto articleDto = new ArticleDto();

        BeanUtil.copyProperties(article, articleDto);
        return articleDto;
    }
}
