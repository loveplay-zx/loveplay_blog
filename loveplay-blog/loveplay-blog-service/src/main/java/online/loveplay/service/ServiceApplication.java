package online.loveplay.service;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.TimeZone;


/**
 * 2022年4月16日09:55:14
 *
 * @author Administrator
 * 创建人： 阿前
 */
@SpringBootApplication
@MapperScan("online.loveplay.service.mapper")
@EnableMBeanExport(registration= RegistrationPolicy.IGNORE_EXISTING)
@Import(FdfsClientConfig.class)
@EnableAsync
public class ServiceApplication {
    public static void main(String[] args) {
        System.out.println("service is starting!");
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
        SpringApplication.run(ServiceApplication.class, args);
        System.out.println("service start success!");
    }



}
