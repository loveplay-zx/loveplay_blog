#  爱玩博客


<p align=center>
   爱玩天下，一个基于微服务架构的前后端分离博客系统
</p>
<p align="center">
<a target="_blank" href="https://gitee.com/loveplay-zx/loveplay_blog">
    	<img src="https://img.shields.io/hexpm/l/plug.svg" ></img>
		<img src="https://img.shields.io/badge/JDK-1.8+-green.svg" ></img>
        <img src="https://img.shields.io/badge/springboot-2.2.2.RELEASE-green" ></img>
<img src="https://img.shields.io/badge/SpringCloud-Hoxton.RELEASE-brightgreen" ></img><img src="https://img.shields.io/badge/vue-2.5.17-green" ></img>
<img src="https://img.shields.io/badge/swagger-3.0.0-brightgreen" ></img>
<img src="https://img.shields.io/badge/mybatis--plus-3.1.2-green" ></img>
</a></p>

##  前言

## 项目介绍

爱玩天下，一个**基于微服务架构的前后端分离博客系统**。**Web** 端使用 **Vue** + **ElementUi** 。后端使用  **SpringBoot** + **Mybatis-plus**进行开发，使用 **Jwt** + **SpringSecurity** 做登录验证和权限校验，使用 **ElasticSearch** 作为全文检索服务，支持文件上传.

- 该工程原来是一个单机服务，这边做成分布式服务，这边做成分布式服务，可集群部署，非常适合作为边学习边输出的一个工程。因能力和时间有限，还有许多可完善的功能还在开发和规划中~
- 服务部署使用jenkins一键打包部署，对于个人开发来说是挺方便的
- 有bug的地方还望支出~

## 运行配置
必须启动的服务包含

`nacos`，`nginx`， `redis`，`elasticSearch`，`mysql`，`loveplay-blog-service`，`loveplay-blog-service`


最低配置：1核2G 【[需开启虚拟内存]】【容易宕机】

推荐配置：2核4G 【[狂欢特惠](https://curl.qcloud.com/O5eFQnIA)】【博主目前配置】

最近，腾讯云和阿里云的优惠力度非常大，如果有需求的小伙伴，可以了解一下~

> 【阿里云】云服务器狂欢特惠，**2核2G5M** 轻量级应用服务器 **60 元/年** [点我传送](https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=ccg9antx)
>
> 【腾讯云】云产品限时秒杀，爆款 **2核4G8M** 云服务器，首年 **74元/年、222/3年**【**博主强烈推荐**】[点我传送](https://curl.qcloud.com/O5eFQnIA)

## 站点演示

> 【演示网址】：http://www.loveplay.online
>

## 项目地址

目前项目托管在 **Gitee** 支持~

- 后端地址：https://gitee.com/loveplay-zx/loveplay_blog
- 前端地址：私聊博主

## 项目目录

- loveplay-blog-api 公用接口层
- loveplay-blog-hunter: 博客爬取
- loveplay-blog-service：服务层
- loveplay-blog-web： 网关控制层

## 技术选型

|      技术      |           说明            |                             官网                             |
| :------------: | :-----------------------: | :----------------------------------------------------------: |
|   SpringBoot   |          MVC框架          | [ https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot) |
| SpringSecurity |      认证和授权框架       |          https://spring.io/projects/spring-security          |
|  MyBatis-Plus  |          ORM框架          |                   https://mp.baomidou.com/                   |
| Elasticsearch  |         搜索引擎          | [ https://github.com/elastic/elasticsearch](https://github.com/elastic/elasticsearch) |
|     Redis      |        分布式缓存         |                      https://redis.io/                       |
|     Docker     |        容器化部署         |      [ https://www.docker.com](https://www.docker.com/)      |
|     Druid      |       数据库连接池        | [ https://github.com/alibaba/druid](https://github.com/alibaba/druid) |
|      JWT       |        JWT登录支持        |                 https://github.com/jwtk/jjwt                 |
|     SLF4J      |         日志框架          |                    http://www.slf4j.org/                     |
|     Lombok     |     简化对象封装工具      | [ https://github.com/rzwitserloot/lombok](https://github.com/rzwitserloot/lombok) |
|     Nginx      |  HTTP和反向代理web服务器  |                      http://nginx.org/                       |
|     Hutool     |      Java工具包类库       |                  https://hutool.cn/docs/#/                   |

## 服务部署

- 待编写中~（可私聊博主帮助部署）
