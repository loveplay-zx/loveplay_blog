package online.loveplay.web.web.controller;

import online.loveplay.api.dto.FriendAddressDto;
import online.loveplay.api.service.FriendAddressService;
import online.loveplay.web.safe.entity.ResponseResult;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2019-10-23
 */
@RestController
@RequestMapping("/friend-address")
public class FriendAddressController {

    @DubboReference
    private FriendAddressService friendAddressService;

    @GetMapping("getFriendAddress")
    public ResponseResult getFriendAddress(){
        ResponseResult result = new ResponseResult();
        List<FriendAddressDto> friendAddresses = friendAddressService.list();
        result.setData(friendAddresses);
        return result;
    }

}
