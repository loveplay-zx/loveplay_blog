package online.loveplay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import online.loveplay.service.bean.IpAddress;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2019-10-31
 */
public interface IpAddressMapper extends BaseMapper<IpAddress> {

    List<IpAddress> selectLists(@Param("example") IpAddress address);
}
