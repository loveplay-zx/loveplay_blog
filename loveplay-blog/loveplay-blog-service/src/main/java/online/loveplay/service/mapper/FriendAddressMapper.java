package online.loveplay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import online.loveplay.service.bean.FriendAddress;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2019-10-23
 */
public interface FriendAddressMapper extends BaseMapper<FriendAddress> {

}
