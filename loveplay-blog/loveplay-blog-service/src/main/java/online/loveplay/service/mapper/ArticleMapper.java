package online.loveplay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import online.loveplay.service.bean.Article;

import java.util.List;


/**
 * @author zx
 */
public interface ArticleMapper extends BaseMapper<Article> {

    List<String> searchTag();

    List<Article> searchTimeLine();
}