package online.loveplay.api.service;

import online.loveplay.api.BusinessException;
import online.loveplay.api.dto.ArticleDto;
import online.loveplay.api.dto.BlogTagDto;
import online.loveplay.api.dto.TimeLineDto;
import online.loveplay.api.vo.ArticleVo;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2019-09-12
 */
public interface ArticleService {


    /**
     * @param articleVo 添加文章
     * @throws BusinessException 异常提示
     */
    void updateArticle(ArticleVo articleVo) throws BusinessException;

    /**
     * @param article 通过各个条件查询全部
     * @throws BusinessException 异常提示
     */
    List<ArticleDto> getArticleByPage(ArticleVo article)throws BusinessException;

    /**
     * @param id 通过id查询
     */
    ArticleDto searchById(int id);

    /**
     * 分页查询
     * @param keywords 搜索框查询
     * @param currentPage 当前页
     * @param pageSize 分页大小
     * @return Article
     */
    List<ArticleDto> getArticleList(String keywords,Integer currentPage,Integer pageSize);


    /**
     * 查询标签墙
     * @return 标签墙
     */
    List<BlogTagDto> searchTag();


    /**
     * 删除文章
     * @param ids
     */
    void deleteArticle(List<Integer> ids)throws BusinessException;

    /**
     * 文章归档
     * @return TimeLineVo
     */
    List<TimeLineDto> searchTimeLine();

    /**
     * 通过文章id和发布者名称查询
     * @param articleId
     * @param managerId
     * @return
     */
    ArticleDto searchByArticleIdAndManagerId(int articleId, int managerId);
}
