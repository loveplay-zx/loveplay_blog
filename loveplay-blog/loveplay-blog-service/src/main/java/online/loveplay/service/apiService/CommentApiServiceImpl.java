package online.loveplay.service.apiService;


import online.loveplay.api.BeanUtils;
import online.loveplay.api.BusinessException;
import online.loveplay.api.dto.CommentDto;
import online.loveplay.api.service.CommentService;
import online.loveplay.api.vo.CommentVo;
import online.loveplay.service.bean.Comment;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2019-09-12
 */
@DubboService
public class CommentApiServiceImpl implements CommentService {

    @Autowired
    private online.loveplay.service.service.CommentService commentService;

    @Override
    public void saveComment(CommentVo commentVo) throws BusinessException {
        Comment comment = (Comment) BeanUtils.copyProperties(commentVo, new Comment());
        commentService.saveComment(comment);
    }

    @Override
    public void deleteComment(Integer id, Integer articleByManagerId) throws BusinessException {
        commentService.deleteComment(id, articleByManagerId);
    }

    @Override
    public List<CommentDto> getCommentsByArticleId(Integer articleId) throws BusinessException {
        List<CommentDto> commentList = commentService.getCommentsByArticleId(articleId);
        return commentList;
    }

    @Override
    public List<CommentDto> getCommentPage(CommentVo comment) throws BusinessException {
        return BeanUtils.copy(commentService.getCommentPage(
                (Comment)BeanUtils.copyProperties(comment, new Comment())
        ), CommentDto.class);
    }

    @Override
    public void delComment(List<Integer> ids) throws BusinessException {
        commentService.delComment(ids);
    }
}
