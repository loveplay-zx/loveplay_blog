package online.loveplay.service.apiService;

import online.loveplay.api.BeanUtils;
import online.loveplay.api.dto.IpAddressDto;
import online.loveplay.api.service.IpAddressService;
import online.loveplay.api.vo.IpAddressVo;
import online.loveplay.service.bean.IpAddress;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2019-10-31
 */
@DubboService
public class IpAddressApiServiceImpl implements IpAddressService {


    @Autowired
    private online.loveplay.service.service.IpAddressService ipAddressService;

    @Override
    public void update(Integer id, String ip) {
        ipAddressService.update(id, ip);
    }

    @Override
    public List<IpAddressDto> selectList(IpAddressVo address) {
        return (List<IpAddressDto>)BeanUtils.copyProperties(ipAddressService.selectList((IpAddress)BeanUtils.copyProperties(address, new IpAddress())),
                new ArrayList<IpAddressDto>());
    }

    @Override
    public List<IpAddressDto> selectIpByManagerId(Integer managerId) {
        return (List<IpAddressDto>)BeanUtils.copyProperties(ipAddressService.selectIpByManagerId(managerId),
                new ArrayList<IpAddressDto>());
    }
}
