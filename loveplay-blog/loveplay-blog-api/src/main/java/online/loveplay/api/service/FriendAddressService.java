package online.loveplay.api.service;


import online.loveplay.api.dto.FriendAddressDto;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2019-10-23
 */
public interface FriendAddressService{

    List<FriendAddressDto> list();
}
