package online.loveplay.service.apiService;

import online.loveplay.api.service.SynExService;
import online.loveplay.service.bean.Article;
import online.loveplay.service.repository.ArticleRepository;
import online.loveplay.service.service.ArticleService;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author ：zx
 * @Date ：10:18 2022/5/10
 * @Description ：手动同步es
 */
@DubboService
public class SynExApiServiceImpl implements SynExService {


    private Logger logger = org.slf4j.LoggerFactory.getLogger(SynExApiServiceImpl.class);
    //注入需要执行任务的service
    @Resource
    private ArticleService articleService;
    @Resource
    private ArticleRepository articleRepository;

    @Override
    public void synEs() {
        logger.info("begin-同步文章数据到es");
        // 删除全部es所以
        articleRepository.deleteAll();
        // 查询全部文章加入到es中
        Article article = new Article();
        List<Article> articleByPage = articleService.getArticleByPage(article);
        articleRepository.saveAll(articleByPage);

        logger.info("end-同步文章数据到es");
    }
}
