package online.loveplay.api.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2019-10-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FriendAddressDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 友链名称
     */
    private String name;

    /**
     * 友链地址
     */
    private String address;


}
