package online.loveplay.service.apiService;

import online.loveplay.api.BeanUtils;
import online.loveplay.api.dto.ManagerDto;
import online.loveplay.api.dto.MusicDto;
import online.loveplay.api.service.MusicService;
import online.loveplay.api.vo.MusicVo;
import online.loveplay.service.bean.Manager;
import online.loveplay.service.bean.Music;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2019-12-11
 */
@DubboService
public class MusicApiServiceImpl implements MusicService {


    @Autowired
    private online.loveplay.service.service.MusicService musicService;

    @Override
    public List<MusicDto> selectList(MusicVo music) {
        return  BeanUtils.copy(musicService.selectList((Music) BeanUtils.copyProperties(music, new Music())), MusicDto.class);
    }
}
