package online.loveplay.service.service;

import online.loveplay.service.bean.FriendAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2019-10-23
 */
public interface FriendAddressService extends IService<FriendAddress> {

}
