package online.loveplay.hunter.config.platform;

import online.loveplay.hunter.config.HunterConfig;

/**
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @since 1.8
 */
public interface InnerPlatform {

    HunterConfig process(String url);
}
