package online.loveplay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import online.loveplay.service.bean.Music;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2019-12-11
 */
public interface MusicMapper extends BaseMapper<Music> {

}
