package online.loveplay.api.constant;

import java.util.concurrent.TimeUnit;

/**
 * @Author ：zx
 * @Date ：16:06 2022/5/10
 * @Description ：常量类
 */
public interface BlogConstant {

    /**
     * 邮箱验证码key
     */
    String EMAIL_CACHE_CODE = "EMAIL_CACHE_CODE";

    /**
     * 邮箱验证码有效时间
     */
    Long EMAIL_CACHE_CODE_EFFECTIVE_TIME = 5l;

    /**
     * 邮箱验证码有效时间
     */
    TimeUnit EMAIL_CACHE_CODE_EFFECTIVE_TIME_UNIT = TimeUnit.MINUTES;
}

