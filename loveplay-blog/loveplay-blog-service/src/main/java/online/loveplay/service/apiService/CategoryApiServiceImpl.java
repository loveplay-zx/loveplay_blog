package online.loveplay.service.apiService;

import cn.hutool.core.bean.BeanUtil;
import online.loveplay.api.BeanUtils;
import online.loveplay.api.dto.CategoryDto;
import online.loveplay.api.service.CategoryService;
import online.loveplay.api.vo.CategoryVo;
import online.loveplay.service.bean.Category;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@DubboService
public class CategoryApiServiceImpl implements CategoryService {

    @Autowired
    private online.loveplay.service.service.CategoryService categoryService;

    @Override
    public List<CategoryDto> findAll() {
        return (List<CategoryDto>)BeanUtils.copyProperties(categoryService.findAll(), new ArrayList<CategoryDto>());
    }

    @Override
    public void saveCategory(CategoryVo category) {
        categoryService.saveCategory((Category) BeanUtils.copyProperties(category, new Category()));
    }

    @Override
    public void delCategory(List<Integer> asList) {
        categoryService.delCategory(asList);
    }

    @Override
    public List<CategoryDto> getCategoryPage(CategoryVo category) {

        return (List<CategoryDto>) BeanUtils.copyProperties(categoryService.getCategoryPage((Category)BeanUtils.copyProperties(category, new Category()) ),
                new ArrayList<CategoryDto>());
    }


}
