package online.loveplay.api.service;

import online.loveplay.api.dto.MusicDto;
import online.loveplay.api.vo.MusicVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2019-12-11
 */
public interface MusicService  {

    List<MusicDto> selectList(MusicVo music);
}
