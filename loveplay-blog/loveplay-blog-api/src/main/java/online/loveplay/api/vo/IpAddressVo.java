package online.loveplay.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2019-10-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class IpAddressVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer managerId;

    /**
     * 访问ip
     */
    private String ip;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 访问时间
     */
    private String loginTime;

    private String managerName;
}
