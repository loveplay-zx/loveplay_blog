package online.loveplay.service.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author ：zx
 * @Date ：11:11 2022/5/9
 * @Description ：标签
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BlogTag implements Serializable {

    private Integer id;
    /**
     * 标签编码
     */
    private String tagCode;
    /**
     * 标签名
     */
    private String tagName;
    /**
     * 创建时间
     */
    private Date createTime;
}
