package online.loveplay.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimeLineDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String year;

    private List<ArticleDto> article;
}
