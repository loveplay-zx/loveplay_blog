package online.loveplay.api.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CategoryDto {

    /**
     * id
     */
    private Integer id;

    /**
     *标题
     */
    private String name;

}
