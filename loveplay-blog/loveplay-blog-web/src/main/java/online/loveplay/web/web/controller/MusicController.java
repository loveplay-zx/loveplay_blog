package online.loveplay.web.web.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import online.loveplay.api.dto.MusicDto;
import online.loveplay.api.service.MusicService;
import online.loveplay.api.vo.MusicVo;
import online.loveplay.web.safe.entity.ResponseResult;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2019-12-11
 */
@RestController
@RequestMapping("/music")
public class MusicController {

    @DubboReference
    private MusicService musicService;

    /**
     * 分页查询
     */
    @GetMapping("/list")
    public ResponseResult getArticlePage(MusicVo music, Integer  pageSize, Integer currentPage){
        ResponseResult result = new ResponseResult();
        if (pageSize != null && currentPage != null){
            PageHelper.startPage(currentPage, pageSize);
            List<MusicDto> musicList= musicService.selectList(music);
            PageInfo<MusicDto> pageInfo = new PageInfo<>(musicList);
            result.setData(pageInfo);
        }else {
            List<MusicDto>  articleList= musicService.selectList(music);
            result.setData(articleList);
        }
        return result;
    }

}
