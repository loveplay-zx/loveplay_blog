app_name='blog-web'
docker stop ${app_name}
echo '-----stop container-----'
docker rm ${app_name}
echo '-----rm container-----'
docker rmi ${app_name}
echo '-----rm none images-----'
docker build -t ${app_name} .
echo '-----build images-----'
docker run -p 8089:8089 --name ${app_name} \
--link nacos:nacos \
--link redis:redis \
-v /home/blog/service/:/home/blog/service/ \
-v /home/blog/service/img/:/home/blog/service/img/ \
-v /usr/local/nginx/html/upfile/:/usr/local/nginx/html/upfile/ \
-d ${app_name}
echo '-----start container-----'
