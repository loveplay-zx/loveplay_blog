package online.loveplay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import online.loveplay.service.bean.Manager;

/**
 * @author zx
 */
public interface ManagerMapper extends BaseMapper<Manager> {

}