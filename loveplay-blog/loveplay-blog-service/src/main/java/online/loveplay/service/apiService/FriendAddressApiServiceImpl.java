package online.loveplay.service.apiService;

import online.loveplay.api.dto.FriendAddressDto;
import online.loveplay.api.service.FriendAddressService;
import online.loveplay.service.bean.FriendAddress;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2019-10-23
 */
@DubboService
public class FriendAddressApiServiceImpl implements FriendAddressService {

    @Autowired
    private online.loveplay.service.service.FriendAddressService friendAddressService;

    @Override
    public List<FriendAddressDto> list() {
        List<FriendAddress> list = friendAddressService.list();
        if(CollectionUtils.isEmpty(list)){
            return null;
        }

        return FriendAddress.toListDto(list);
    }
}
