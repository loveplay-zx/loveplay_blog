package online.loveplay.web.web.controller;

import online.loveplay.api.dto.CategoryDto;
import online.loveplay.api.service.CategoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import online.loveplay.api.BusinessException;
import online.loveplay.api.vo.CategoryVo;
import online.loveplay.web.safe.entity.ResponseResult;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2019-09-12
 */
@RestController
@RequestMapping("/category")
public class CategoryController {
    @DubboReference
    private CategoryService categoryService;


    /**
     * 新增分类
     * @param category 分类内容
     * @return result
     */
    @ResponseBody
    @PostMapping("/save")
    public ResponseResult saveCategory(CategoryVo category){
        ResponseResult result = new ResponseResult();
        //新增分类
        categoryService.saveCategory(category);
        return result;
    }


    /**
     * 分页查询
     */
    @GetMapping("/list")
    public ResponseResult getCategoryPage(CategoryVo category, Integer pageSize, Integer currentPage){
        ResponseResult result = new ResponseResult();
        if (pageSize != null && currentPage != null){
            PageHelper.startPage(currentPage, pageSize);
            List<CategoryDto>  categorys= categoryService.getCategoryPage(category);
            PageInfo<CategoryDto> pageInfo = new PageInfo<>(categorys);
            result.setData(pageInfo);
        }else {
            List<CategoryDto>  categorys= categoryService.getCategoryPage(category);
            result.setData(categorys);
        }
        return result;
    }

    /**
     * 通过id删除
     */
    @DeleteMapping("/deletes")
    public ResponseResult delCategory(Integer[] ids) throws BusinessException {
        ResponseResult result = new ResponseResult();
        categoryService.delCategory(Arrays.asList(ids));
        return result;
    }
}
