package online.loveplay.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import online.loveplay.service.bean.Category;

import java.util.List;

public interface CategoryService extends IService<Category> {

    List<Category> findAll();

    void saveCategory(Category category);

    void delCategory(List<Integer> asList);

    List<Category> getCategoryPage(Category category);
}
