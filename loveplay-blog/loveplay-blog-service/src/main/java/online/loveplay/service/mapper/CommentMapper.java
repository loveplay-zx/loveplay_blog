package online.loveplay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import online.loveplay.service.bean.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2019-09-12
 */
public interface CommentMapper extends BaseMapper<Comment> {

    List<Comment> selectListByArticleId(@Param("articleId") Integer articleId);

    int save(@Param("comment") Comment comment);
}
