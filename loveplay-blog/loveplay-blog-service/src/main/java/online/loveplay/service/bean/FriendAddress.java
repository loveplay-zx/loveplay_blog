package online.loveplay.service.bean;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import online.loveplay.api.dto.FriendAddressDto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author zx
 * @since 2019-10-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("blog_friend_address")
public class FriendAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 友链名称
     */
    private String name;

    /**
     * 友链地址
     */
    private String address;


    public FriendAddressDto toDto(){
        FriendAddressDto friendAddressDto = new FriendAddressDto();
        BeanUtil.copyProperties(this, friendAddressDto);
        return friendAddressDto;
    }

    public static List<FriendAddressDto> toListDto(List<FriendAddress> list){
        if(CollectionUtil.isEmpty(list)){
            return null;
        }
        List<FriendAddressDto> dtoList = new ArrayList<>();
        for (FriendAddress friendAddress : list) {
            FriendAddressDto friendAddressDto = new FriendAddressDto();
            BeanUtil.copyProperties(friendAddress, friendAddressDto);
            dtoList.add(friendAddressDto);
        }
        return dtoList;
    }
}
