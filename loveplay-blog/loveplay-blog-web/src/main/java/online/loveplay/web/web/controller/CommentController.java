package online.loveplay.web.web.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import online.loveplay.api.BusinessException;
import online.loveplay.api.dto.CommentDto;
import online.loveplay.api.dto.ManagerDto;
import online.loveplay.api.service.CommentService;
import online.loveplay.api.vo.CommentVo;
import online.loveplay.web.safe.entity.ResponseResult;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2019-09-12
 */
@RestController
@RequestMapping("/comment")
public class CommentController extends BaseController{

    @DubboReference
    private CommentService commentService;

    /**
     *
     */
    @ResponseBody
    @GetMapping(value = "{articleId}")
    public ResponseResult getComment(@PathVariable(value = "articleId") int articleId){
        ResponseResult result = new ResponseResult();
        List<CommentDto> comment =commentService.getCommentsByArticleId(articleId);
        result.setData(comment);
        return result;
    }

    /**
     * 新增评论
     * @param comment 评论内容
     * @return result
     */
    @ResponseBody
    @PostMapping("/save")
    public ResponseResult saveComment(CommentVo comment){
        ResponseResult result = new ResponseResult();
        ManagerDto manager = getManager();
        comment.setByManagerId(manager.getId());
        //新增评论
        commentService.saveComment(comment);
        return result;
    }

    /**
     * 删除评论
     * @param id  评论id
     * @param articleByManagerId  作者id
     * @return result
     */
    @ResponseBody
    @DeleteMapping("/delete")
    public ResponseResult deleteComment(Integer id,Integer articleByManagerId){
        ResponseResult result = new ResponseResult();
        //删除评论
        commentService.deleteComment(id,articleByManagerId);
        return result;
    }

    /**
     * 分页查询
     */
    @GetMapping("/list")
    public ResponseResult getCommentPage(CommentVo comment, Integer pageSize, Integer currentPage){
        ResponseResult result = new ResponseResult();
        if (pageSize != null && currentPage != null){
            PageHelper.startPage(currentPage, pageSize);
            List<CommentDto>  comments= commentService.getCommentPage(comment);
            PageInfo<CommentDto> pageInfo = new PageInfo<>(comments);
            result.setData(pageInfo);
        }else {
            List<CommentDto>  comments= commentService.getCommentPage(comment);
            result.setData(comments);
        }
        return result;
    }

    /**
     * 通过id删除
     */
    @DeleteMapping("/deletes")
    public ResponseResult delComment(Integer[] ids) throws BusinessException {
        ResponseResult result = new ResponseResult();
        commentService.delComment(Arrays.asList(ids));
        return result;
    }

}
