package online.loveplay.api.service;


import online.loveplay.api.dto.CategoryDto;
import online.loveplay.api.vo.CategoryVo;

import java.util.List;

public interface CategoryService  {

    List<CategoryDto> findAll();

    void saveCategory(CategoryVo category);

    void delCategory(List<Integer> asList);

    List<CategoryDto> getCategoryPage(CategoryVo category);
}
