package online.loveplay.web.safe.config;

import com.alibaba.fastjson.JSON;
import online.loveplay.web.safe.entity.ResponseResult;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.springframework.security.access.AccessDeniedException;

/**
 * @Author ：zx
 * @Date ：14:03 2022/5/6
 * @Description ：授权失败
 */
@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        ResponseResult result = new ResponseResult(403, "您的权限不足！");
        String json = JSON.toJSONString(result);
        // 将字符串渲染到客户端
        WebUtils.renderString(response, json);
    }

}
