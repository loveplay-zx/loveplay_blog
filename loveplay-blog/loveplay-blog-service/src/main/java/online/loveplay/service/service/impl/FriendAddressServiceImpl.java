package online.loveplay.service.service.impl;

import online.loveplay.service.bean.FriendAddress;
import online.loveplay.service.mapper.FriendAddressMapper;
import online.loveplay.service.service.FriendAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import online.loveplay.api.BusinessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2019-10-23
 */
@Service
@Transactional(rollbackFor= BusinessException.class)
public class FriendAddressServiceImpl extends ServiceImpl<FriendAddressMapper, FriendAddress> implements FriendAddressService {

}
