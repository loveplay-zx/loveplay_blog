package online.loveplay.web.web.controller;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import com.alibaba.fastjson.JSONArray;
import online.loveplay.api.ThreadPoolUtil;
import online.loveplay.api.dto.ManagerDto;
import online.loveplay.api.service.ArticleService;
import online.loveplay.api.vo.ArticleVo;
import online.loveplay.hunter.consts.HunterConsts;
import online.loveplay.hunter.entity.ImageLink;
import online.loveplay.hunter.entity.VirtualArticle;
import online.loveplay.hunter.processor.BlogHunterProcessor;
import online.loveplay.hunter.processor.HunterProcessor;
import online.loveplay.hunter.util.PlatformUtil;
import online.loveplay.web.safe.entity.ResponseResult;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Author ：zx
 * @Date ：13:19 2022/5/5
 * @Description ：博客爬取
 */
@RestController
@RequestMapping("/hunter")
public class HunterController extends BaseController{

    private static final Logger log = LoggerFactory.getLogger(HunterController.class);

    @DubboReference
    private ArticleService articleService;


    @RequestMapping("/pull")
    public ResponseResult pull(String url, String tags) {
        ResponseResult result = new ResponseResult();
        CopyOnWriteArrayList<VirtualArticle> virtualArticles = single( url, false);
        result.setData(virtualArticles);
        save(virtualArticles, tags);
        return result;
    }
    private void save(CopyOnWriteArrayList<VirtualArticle> virtualArticles, String tags){
        ManagerDto managerDto = getManager();
        if(virtualArticles != null && virtualArticles.size() > 0){
            virtualArticles.forEach(virtualArticle ->{
                ArticleVo articleVo = new ArticleVo();
                articleVo.setArticleName(virtualArticle.getTitle());
                String articleHeadPic = "";
                Set<ImageLink> linkSet = virtualArticle.getImageLinks();
                if(linkSet!= null && linkSet.size()>0){
                    for (ImageLink link : linkSet) {
                        articleHeadPic = link.getSrcLink();
                        break;
                    }
                }
                articleVo.setArticleHeadPic(articleHeadPic);
                articleVo.setArticleTag(tags);
                articleVo.setArticleRemark(virtualArticle.getDescription());
                articleVo.setArticleReadCount(0);
                articleVo.setArticleState(1);
                articleVo.setArticleContent(virtualArticle.getContent());
                articleVo.setManagerId(managerDto.getId());
                articleVo.setManagerName(managerDto.getName());
                articleVo.setCreateTime(DateTime.now().toString(DatePattern.NORM_DATETIME_MINUTE_PATTERN));
                articleVo.setArticleType(0);
                articleVo.setArticleStarNum(0);
                articleVo.setArticleConNum(0);
                articleVo.setEnclosure("");
                articleVo.setLatest(true);
                articleVo.setFavorite(true);
                articleVo.setCommentMost(true);
                articleVo.setRecommend(true);
                ThreadPoolUtil.execute(new Runnable() {
                    @Override
                    public void run() {
                        articleService.updateArticle(articleVo);
                    }
                });

            } );

        }
    }
    /**
     * 抓取单个文章
     *
     * @param url          文件地址
     * @param convertImage 是否转存图片，当选择true时会在结果中返回该文中的所有图片链接
     */
    private CopyOnWriteArrayList<VirtualArticle> single(String url, boolean convertImage) {
        log.info(HunterConsts.LOG_PREFIX + url + " | " + PlatformUtil.getDomain(url) + " | " + PlatformUtil.getHost(url));
        HunterProcessor hunter = new BlogHunterProcessor(url, convertImage);
        CopyOnWriteArrayList<VirtualArticle> list = hunter.execute();
        if (null == list || list.isEmpty()) {
            log.info("没获取到数据: {}", url);
        } else {
            this.check(list);
        }
        return list;
    }

    private void check(CopyOnWriteArrayList<VirtualArticle> list) {
        for (VirtualArticle virtualArticle : list) {
            log.info(HunterConsts.LOG_PREFIX + JSONArray.toJSONString(virtualArticle.getImageLinks()));
            if (StringUtils.isEmpty(virtualArticle.getContent())) {
                log.error(HunterConsts.LOG_PREFIX + "内容为空");
            }
            if (StringUtils.isEmpty(virtualArticle.getAuthor())) {
                log.error(HunterConsts.LOG_PREFIX + "作者为空");
            }
            if (StringUtils.isEmpty(virtualArticle.getSource())) {
                log.error(HunterConsts.LOG_PREFIX + "源站为空");
            }
            if (StringUtils.isEmpty(virtualArticle.getDescription())) {
                log.error(HunterConsts.LOG_PREFIX + "Description为空");
            }
            if (StringUtils.isEmpty(virtualArticle.getKeywords())) {
                log.error(HunterConsts.LOG_PREFIX + "Keywords内容为空");
            }
            if (StringUtils.isEmpty(virtualArticle.getTitle())) {
                log.error(HunterConsts.LOG_PREFIX + "标题为空");
            }
            if (null == virtualArticle.getReleaseDate()) {
                log.error(HunterConsts.LOG_PREFIX + "发布日期为空");
            }
            if (CollectionUtils.isEmpty(virtualArticle.getTags())) {
                log.error(HunterConsts.LOG_PREFIX + "标签为空");
            }
        }
    }

}
