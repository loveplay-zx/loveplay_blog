package online.loveplay.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zx
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ManagerVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 用户名 邮箱
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 临时密码
     */
    private String agiPassword;

    /**
     * 头像
     */
    private String headPic;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 角色  超级管理员 管理员 博主
     */
    private String type;

    /**
     * 验证码
     */
    private Integer code;

}