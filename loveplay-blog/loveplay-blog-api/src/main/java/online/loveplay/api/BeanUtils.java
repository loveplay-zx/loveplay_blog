package online.loveplay.api;

import cn.hutool.core.bean.BeanUtil;

import com.alibaba.fastjson.JSON;
import java.util.List;
import org.springframework.util.CollectionUtils;

/**
 * @Author ：zx
 * @Date ：11:09 2022/4/21
 * @Description ：
 */
public class BeanUtils {
    public static Object copyProperties(Object source, Object target){
        BeanUtil.copyProperties(source, target);
        return target;
    }
    public static <T> List<T> copy(List<?> list, Class<T> clazz) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        } else {
            String oldOb = JSON.toJSONString(list);
            return JSON.parseArray(oldOb, clazz);
        }
    }
}
