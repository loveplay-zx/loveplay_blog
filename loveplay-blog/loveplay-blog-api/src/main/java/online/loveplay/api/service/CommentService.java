package online.loveplay.api.service;


import online.loveplay.api.BusinessException;
import online.loveplay.api.dto.CommentDto;
import online.loveplay.api.vo.CommentVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2019-09-12
 */
public interface CommentService  {

    /**
     * @param comment 新增评论
     * @throws BusinessException 异常处理
     */
    void saveComment(CommentVo comment)throws BusinessException;

    /**
     * 删除评论
     * @param id 通过id删除
     * @param articleByManagerId 文章作者id
     * @throws BusinessException 异常处理
     */
    void deleteComment(Integer id,Integer articleByManagerId)throws BusinessException;

    /**
     * 通过文章id获取评论
     * @param articleId 文章id
     * @return  List<Comment>
     */
    List<CommentDto> getCommentsByArticleId(Integer articleId)throws BusinessException;

    /**
     * 评论列表
     * @param comment
     * @return
     */
    List<CommentDto> getCommentPage(CommentVo comment)throws BusinessException;

    /**
     * 通过id删除
     * @param asList
     * @throws BusinessException
     */
    void delComment(List<Integer> ids)throws BusinessException;
}
