package online.loveplay.api;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : Latent
 * @createdDate : 2019/8/12
 * @updatedDate
 */
@Data
public class BaseTreeNode implements Serializable {
    /**
     * 子Id
     */
    private Object id;
    /**
     * 父ID
     */

    /**
     * 被评论人
     */
    private Object pid;

    private List<BaseTreeNode> replyComments;

    public BaseTreeNode() {
    }


    public List<BaseTreeNode> getReplyComments() {
        return this.replyComments;
    }

    public void setReplyComments(List<BaseTreeNode> child) {
        this.replyComments = child;
    }

    public void addReplyComments(BaseTreeNode baseTreeNode) {
        if (this.replyComments == null) {
            this.setReplyComments(new ArrayList());
        }

        this.getReplyComments().add(baseTreeNode);
    }
}
