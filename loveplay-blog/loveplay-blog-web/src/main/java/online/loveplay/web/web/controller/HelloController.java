package online.loveplay.web.web.controller;

import com.alibaba.fastjson.JSONObject;
import online.loveplay.api.service.SynExService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class HelloController {

    @DubboReference
    private SynExService synExService;

    @PostMapping("/save_error_logger")
    public void sayHello(Map<String, Object> params){
        System.out.println(JSONObject.toJSONString(params));
    }

    @PostMapping("/syn_es")
    public void synEs(){
        synExService.synEs();
    }
}
