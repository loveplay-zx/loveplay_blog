package online.loveplay.api.vo;

import lombok.Data;

@Data
public class CategoryVo {

    /**
     * id
     */
    private Integer id;

    /**
     *标题
     */
    private String name;

}
