package online.loveplay.api.service;


import online.loveplay.api.BusinessException;
import online.loveplay.api.dto.ManagerDto;
import online.loveplay.api.vo.ManagerVo;

import java.util.List;

/**
 * @author zx
 */
public interface ManagerService {
    /**
     * 通过用户名查询
     * @param userName
     * @return
     */
    ManagerDto findByName(String userName)throws BusinessException;

    /**
     * 保存用户
     * @param manager
     * @throws BusinessException
     */
    ManagerDto saveManager(ManagerVo manager)throws BusinessException;

    /**
     * 向用户发送邮箱
     * @param username
     */
    void sendOutEmail(String username)throws BusinessException;

    /**
     * 查询全部
     * @param manager
     * @return
     */
    List<ManagerDto> getManagerByPage(ManagerVo manager);

    /**
     * 删除用户
     * @param ids
     */
    void delManager(List<Integer> ids)throws BusinessException;

    /**
     * 禁用用户
     * @param ids
     * @param key
     */
    void stopManager(List<Integer> ids, int key)throws BusinessException;

    /**
     * 修改管理员
     * @param manager
     * @return
     */
    ManagerDto updateManager(ManagerVo manager);
}
