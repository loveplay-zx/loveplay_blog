package online.loveplay.service.apiService;

import online.loveplay.api.BeanUtils;
import online.loveplay.api.dto.ManagerDto;
import online.loveplay.api.vo.ManagerVo;
import online.loveplay.api.service.ManagerService;
import online.loveplay.api.BusinessException;
import online.loveplay.service.bean.Manager;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zx
 */
@DubboService
public class ManagerApiServiceImpl implements ManagerService {


    @Autowired
    private online.loveplay.service.service.ManagerService managerService;

    @Override
    public ManagerDto findByName(String userName) throws BusinessException {
        return (ManagerDto) BeanUtils.copyProperties(managerService.findByName(userName), new ManagerDto());
    }

    @Override
    public ManagerDto saveManager(ManagerVo manager) throws BusinessException {
        return (ManagerDto) BeanUtils.copyProperties(managerService.saveManager((Manager) BeanUtils.copyProperties(manager, new Manager())), new ManagerDto());
    }

    @Override
    public void sendOutEmail(String username) throws BusinessException {
        managerService.sendOutEmail(username);
    }

    @Override
    public List<ManagerDto> getManagerByPage(ManagerVo manager) {
        List<Manager> list = managerService.getManagerByPage((Manager) BeanUtils.copyProperties(manager, new Manager()));
        List<ManagerDto> dtoList = new ArrayList<>();
        for(Manager manager1 : list){
            ManagerDto dto = new ManagerDto();
            BeanUtils.copyProperties(manager1, dto);
            dtoList.add(dto);
        }
        return dtoList;

    }

    @Override
    public void delManager(List<Integer> ids) throws BusinessException {
        managerService.delManager(ids);
    }

    @Override
    public void stopManager(List<Integer> ids, int key) throws BusinessException {
        managerService.stopManager(ids, key);
    }

    @Override
    public ManagerDto updateManager(ManagerVo manager) {
        return (ManagerDto) BeanUtils.copyProperties(managerService.updateManager((Manager) BeanUtils.copyProperties(manager, new Manager())), new ManagerDto());

    }
}
