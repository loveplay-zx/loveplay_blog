package online.loveplay.web.web.comment;

import javax.servlet.http.HttpServletRequest;

import online.loveplay.api.BusinessException;
import online.loveplay.web.safe.entity.ResponseResult;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;


/**
 * 切面
 *
 * @ClassName: LogRecordAspect
 * @author gbq
 */
@Aspect
@Component
@Order(1)
public class LogRecordAspect {

    private static final Logger logger = LoggerFactory.getLogger(LogRecordAspect.class);

    @Pointcut("execution(* online.loveplay.web.web.controller..*(..))")
    public void excudeService() {
    }

    @SuppressWarnings("unchecked")
    @Around(value = "excudeService()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        String url = request.getRequestURI();
        String method = request.getMethod();
        String uri = request.getRequestURI();
        String queryString = request.getQueryString();
        logger.info("接收, url: {}, method: {}, uri: {}, params: {}", url, method, uri,
                queryString == null ? "" : queryString);
        ResponseResult result = new ResponseResult();
        try {
            Object object = pjp.proceed();
            result.setStatus(0);
            if (object instanceof String || object instanceof ModelAndView) {
                return object;
            } else if (object instanceof ResponseResult) {
                result = (ResponseResult) object;
                result.setStatus(0);
            } else if (object instanceof JSONObject) {
                JSONObject jsonObject = (JSONObject) object;
                result.setData(jsonObject);
            }
        } catch (BusinessException exception) {
            result = new ResponseResult();
            result.setMsg( exception.getErrorMessage());
            result.setData(null);
            result.setStatus(-1);
            exception.printStackTrace();
           logger.error("error, message: {}, errorMessage: {}, exception: {}", exception.getMessage());
            } catch (Exception e) {
            result = new ResponseResult();
            result.setMsg(  "系统错误");
            result.setStatus(-1);
            result.setData(null);
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        logger.info("响应, url: {}, result: {}", url, result);
        return result;
    }
}
