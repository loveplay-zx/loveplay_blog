app_name='blog-service'
docker stop ${app_name}
echo '-----stop container-----'
docker rm ${app_name}
echo '-----rm container-----'
docker rmi ${app_name}
echo '-----rm none images-----'
docker build -t ${app_name} .
echo '-----build images-----'
docker run -p 8087:8087 --name ${app_name} \
--link mysql57:db \
--link elasticsearch:es \
--link nacos:nacos \
--link redis:redis \
-v /home/blog/service/:/home/blog/service/ \
-d ${app_name}
echo '-----start container-----'
