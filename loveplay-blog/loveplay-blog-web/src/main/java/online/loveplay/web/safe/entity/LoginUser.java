package online.loveplay.web.safe.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.loveplay.api.dto.ManagerDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author ：zx
 * @Date ：14:26 2022/5/6
 * @Description ：登录用户
 */
@Data
@NoArgsConstructor
public class LoginUser implements UserDetails {

    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 用户名 邮箱
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 临时密码
     */
    private String agiPassword;

    /**
     * 头像
     */
    private String headPic;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 角色  超级管理员 管理员 博主
     */
    private String type;

    /**
     * 验证码
     */
    private Integer code;


    public void managerDtoToLoginUser(ManagerDto managerDto){
       this.setId(managerDto.getId());
       this.setName(managerDto.getName());
       this.setUsername(managerDto.getUsername());
       this.setPassword(managerDto.getPassword());
       this.setAgiPassword(managerDto.getAgiPassword());
       this.setHeadPic(managerDto.getHeadPic());
       this.setCreateTime(managerDto.getCreateTime());
       this.setStatus(managerDto.getStatus());
       this.setType(managerDto.getType());
       this.setCode(managerDto.getCode());
    }

    private List<String> permissions;//权限信息

    @JSONField(serialize = false) //fastjson注解,表示此属性不会被序列化，因为SimpleGrantedAuthority这个类型不能在redis中序列化
    private List<SimpleGrantedAuthority> authorities;

    /**
     * 获取权限信息
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
      /*  // 权限为空的时候才往遍历，不为空直接返回
        if (authorities != null) {
            return authorities;
        }
        //把permissions中String类型的权限信息封装成SimpleGrantedAuthority对象
        authorities = new ArrayList<>();
        for (String permission : permissions) {
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(permission);
            authorities.add(authority);
        }*/
        return authorities;
    }

    /**
     * 获取密码
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * 获取用户名
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * 判断是否过期
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 是否锁定
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 是否没有超时
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否可用
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
