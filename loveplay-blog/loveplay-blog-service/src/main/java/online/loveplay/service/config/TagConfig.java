package online.loveplay.service.config;

import online.loveplay.service.bean.BlogTagMap;
import online.loveplay.service.mapper.BlogTagMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * @Author ：zx
 * @Date ：14:57 2022/5/10
 * @Description ：标签配置
 */
@Configuration
public class TagConfig {

    @Resource
    private BlogTagMapper blogTagMapper;

    @Bean
    public BlogTagMap init(){
        BlogTagMap blogTagMap = new BlogTagMap();
        blogTagMap.setMap(new HashMap<>());
        blogTagMap.addAll(blogTagMapper.list());
        return blogTagMap;
    }
}
