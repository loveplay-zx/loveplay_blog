package online.loveplay.web.web.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import online.loveplay.api.ConstUtil;
import online.loveplay.api.dto.ManagerDto;
import online.loveplay.api.enums.DataStatusEnum;
import online.loveplay.api.service.ManagerService;
import online.loveplay.api.vo.ManagerVo;
import online.loveplay.web.safe.entity.ResponseResult;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * @author zx
 */
@RestController
@RequestMapping("/manager")
public class ManagerController extends BaseController{

    @DubboReference
    private ManagerService managerService;

    /**
     * 获取当前登录人
     */
    @GetMapping("/getCurrentManager")
    public ResponseResult getCurrentManager(){

        ResponseResult result = new ResponseResult();
        ManagerDto manager =  getManager();
        result.setData(manager);
        return result;
    }

    /**
     * 分页查询
     */
    @ResponseBody
    @GetMapping("/list")
    public ResponseResult getArticlePage(ManagerVo manager, Integer pageSize, Integer currentPage){
        ResponseResult result = new ResponseResult();
        ManagerDto manager1 =  getManager();

        if (manager1.getType().equals(ConstUtil.ADMIN)){
            manager.setType(ConstUtil.BOLG_MIN);
        }

        if (pageSize != null && currentPage != null){
            PageHelper.startPage(currentPage, pageSize);
            List<ManagerDto> articleList= managerService.getManagerByPage(manager);
            PageInfo<ManagerDto> pageInfo = new PageInfo<>(articleList);
            result.setData(pageInfo);
        }else {
            List<ManagerDto>  articleList= managerService.getManagerByPage(manager);
            result.setData(articleList);
        }
        return result;
    }

    /**
     * 通过id删除
     */
    @DeleteMapping("/delete")
    public ResponseResult delManager(Integer[] ids){
        ResponseResult result = new ResponseResult();
        managerService.delManager(Arrays.asList(ids));
        return result;
    }

    /**
     * 批量禁用用户
     */
    @PutMapping("/stop")
    public ResponseResult stopManager(Integer[] ids){
        ResponseResult result = new ResponseResult();
        managerService.stopManager(Arrays.asList(ids), DataStatusEnum.STATUS_STOP.getKey());
        return result;
    }

    /**
     * 批量启用用户
     */
    @PutMapping("/star")
    public ResponseResult starManager(Integer[] ids){
        ResponseResult result = new ResponseResult();
        managerService.stopManager(Arrays.asList(ids),DataStatusEnum.STATUS_START.getKey());
        return result;
    }

    /**
     * 添加管理员
     * @param manager 注册用户信息
     * @return result
     */
    @PostMapping("/save")
    public ResponseResult save(ManagerVo manager) {
        ResponseResult result = new ResponseResult();
        manager.setType("管理员");
        ManagerDto manager1 = managerService.saveManager(manager);
        result.setData(manager1);
        return result;
    }

    /**
     * 修改管理员
     * @param manager 注册用户信息
     * @return result
     */
    @PostMapping("/update")
    public ResponseResult update(ManagerVo manager) {
        ResponseResult result = new ResponseResult();
        ManagerDto manager1 = managerService.updateManager(manager);
        result.setData(manager1);
        return result;
    }
}
