package online.loveplay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import online.loveplay.service.bean.BlogTag;

import java.util.List;

/**
 * @Author ：zx
 * @Date ：11:10 2022/5/9
 * @Description ：标签
 */
public interface BlogTagMapper extends BaseMapper<BlogTag> {

    List<BlogTag> list();
}
