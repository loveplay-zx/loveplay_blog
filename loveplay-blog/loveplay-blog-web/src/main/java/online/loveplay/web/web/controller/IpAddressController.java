package online.loveplay.web.web.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import online.loveplay.api.dto.IpAddressDto;
import online.loveplay.api.service.IpAddressService;
import online.loveplay.api.vo.IpAddressVo;
import online.loveplay.web.safe.entity.ResponseResult;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2019-10-31
 */
@RestController
@RequestMapping("/ip-address")
public class IpAddressController {

    @DubboReference
    private IpAddressService ipAddressService;

    /**
     * 分页查询
     */
    @GetMapping("/list")
    public ResponseResult getArticlePage(IpAddressVo address, Integer  pageSize, Integer currentPage){
        ResponseResult result = new ResponseResult();
        if (pageSize != null && currentPage != null){
            PageHelper.startPage(currentPage, pageSize);
            List<IpAddressDto> articleList= ipAddressService.selectList(address);
            PageInfo<IpAddressDto> pageInfo = new PageInfo<>(articleList);
            result.setData(pageInfo);
        }else {
            List<IpAddressDto>  articleList= ipAddressService.selectList(address);
            result.setData(articleList);
        }
        return result;
    }


    /**
     * 根据codeId查询订单
     */
    @GetMapping("/{managerId}")
    public ResponseResult selectIpByManagerId(@PathVariable(value = "managerId") Integer managerId) {
        ResponseResult result = new ResponseResult();
        // 查询数据
        List<IpAddressDto> ipAddresses=ipAddressService.selectIpByManagerId(managerId);
        result.setData(ipAddresses);
        return result;
    }

}
