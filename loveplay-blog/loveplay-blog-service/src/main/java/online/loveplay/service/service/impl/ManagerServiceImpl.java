package online.loveplay.service.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import online.loveplay.api.constant.BlogConstant;
import online.loveplay.service.bean.Manager;
import online.loveplay.service.mapper.ManagerMapper;
import online.loveplay.service.service.ManagerService;
import online.loveplay.api.BusinessException;
import online.loveplay.api.ConstUtil;
import online.loveplay.api.enums.DataStatusEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Random;

/**
 * @author zx
 */
@Service
@Transactional(rollbackFor=BusinessException.class)
public class ManagerServiceImpl  extends ServiceImpl<ManagerMapper, Manager> implements ManagerService {
    @Resource
    private ManagerMapper managerMapper;
    @Resource
    private JavaMailSender javaMailSender;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Manager findByName(String username)throws BusinessException  {
        //通过用户名查询
        QueryWrapper<Manager> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Manager::getUsername,username)
                .eq(Manager::getStatus,ConstUtil.DEFAULT_STATE);
        return  managerMapper.selectOne(queryWrapper);
    }

    @Override
    public Manager saveManager(Manager manager) throws BusinessException {
        try {
            if (manager.getId()==null){
                manager.setCreateTime(DateUtil.now());
            }

            //先验证邮箱是否已经被注册
            Manager byName = this.findByName(manager.getUsername());
            if (byName != null){
                throw new BusinessException("已经注册，不能再次注册。");
            }

                String cacheCode  = (String) redisTemplate.opsForValue().get(BlogConstant.EMAIL_CACHE_CODE+manager.getUsername());
                if (cacheCode == null){
                    throw new BusinessException("验证码已经失效。");
                }

                int code = Integer.parseInt(cacheCode);
                if(manager.getCode() != code){
                    throw new BusinessException("验证码输入错误。");
                }

            redisTemplate.delete(cacheCode);


            manager.setPassword(new BCryptPasswordEncoder().encode(manager.getPassword()));
            managerMapper.insert(manager);
            return manager;
        } catch (BusinessException e) {
            throw new BusinessException(e.getErrorMessage());
        }
    }

    @Override
    public void sendOutEmail(String username) throws BusinessException{
        try {
            //先验证邮箱是否已经被注册
            Manager byName = this.findByName(username);
            if (byName != null){
                throw new BusinessException("邮箱已经注册，不能再次注册。");
            }

            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("loveplayzx@163.com");
            helper.setTo(username);
            helper.setSubject("标题：请收好你在LOVE PLAY的博客的注册码");

            String checkCode = String.valueOf(new Random().nextInt(899999) + 100000);
            redisTemplate.opsForValue().set(BlogConstant.EMAIL_CACHE_CODE+username, checkCode,
                    BlogConstant.EMAIL_CACHE_CODE_EFFECTIVE_TIME, BlogConstant.EMAIL_CACHE_CODE_EFFECTIVE_TIME_UNIT);

            String str ="<h4>欢迎您在LOVE PLAY的博客注册</h4>"+"<p style='color:#F00'>您的验证码为:"+checkCode+"</p>";
            //append("<p style='text-align:right'>右对齐</p>");
            helper.setText(str, true);

       /* FileSystemResource fileSystemResource=new FileSystemResource(new File("C:\\Users\\28937\\Desktop\\搞笑图片\\welcome.gif"));
        helper.addAttachment("啦啦啦",fileSystemResource);*/
            javaMailSender.send(message);
        } catch (BusinessException e) {
            throw new BusinessException(e.getErrorMessage());
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Manager> getManagerByPage(Manager manager) {
        QueryWrapper<Manager> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().like(StringUtils.isNotBlank(manager.getUsername()),Manager::getUsername,manager.getUsername())
                             .like(StringUtils.isNotBlank(manager.getName()),Manager::getName,manager.getName())
                             .eq(StringUtils.isNotBlank(manager.getType()),Manager::getType,manager.getType())
                             .orderByDesc(Manager::getCreateTime)
                             .orderByDesc(Manager::getStatus);
        List<Manager> list = managerMapper.selectList(queryWrapper);
        return list;
    }

    @Override
    public void delManager(List<Integer> ids) throws BusinessException{
        try {
            managerMapper.deleteBatchIds(ids);
        }catch (BusinessException e){
            throw new BusinessException(e.getErrorMessage());
        }
    }

    @Override
    public void stopManager(List<Integer> ids, int key)throws BusinessException{
        try {
            List<Manager> managers = managerMapper.selectBatchIds(ids);
            if(managers.stream().anyMatch(x->x.getStatus()==key)){
                throw new BusinessException("已经是"+ DataStatusEnum.getValue(key)+"状态");
            }
            managers.forEach(x->x.setStatus(key));
            this.updateBatchById(managers);
        }catch (BusinessException e){
            throw new BusinessException(e.getErrorMessage());
        }
    }

    @Override
    public Manager updateManager(Manager manager) {
        QueryWrapper<Manager> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().ne(Manager::getId,manager.getId())
                             .eq(Manager::getUsername,manager.getUsername())
                             .eq(Manager::getStatus, ConstUtil.DEFAULT_STATE);

        if (managerMapper.selectCount(queryWrapper)>0){
             throw new BusinessException("用户名或者昵称已经存在");
        }

        if (manager.getPassword()!=null){
            manager.setPassword(new BCryptPasswordEncoder().encode(manager.getPassword()));
        }

        managerMapper.updateById(manager);
        return manager;
    }

}
