package online.loveplay.api;

import java.util.concurrent.*;

/**
 * @Author ：zx
 * @Date ：11:06 2022/5/7
 * @Description ：符合阿里巴巴规范的线程池
 */
public class ThreadPoolUtil {
    public static ThreadPoolExecutor threadPool;

    /**
     * 无返回值直接执行
     *
     * @param runnable
     */
    public static void execute(Runnable runnable) {
        getThreadPool().execute(runnable);
    }

    /**
     * 返回值直接执行
     *
     * @param callable
     */
    public static <T> Future<T> submit(Callable<T> callable) {
        return getThreadPool().submit(callable);
    }

    /**
     * 关闭线程池
     */
    public static void shutdown() {
        getThreadPool().shutdown();
    }

    /**
     * dcs获取线程池
     *
     * @return 线程池对象
     */
    public static ThreadPoolExecutor getThreadPool() {
        if (threadPool != null) {
            return threadPool;
        } else {
            synchronized (ThreadPoolUtil.class) {
                if (threadPool == null) {
                    threadPool = new ThreadPoolExecutor(8, 16, 60, TimeUnit.SECONDS,
                            new LinkedBlockingQueue<>(32), new ThreadPoolExecutor.CallerRunsPolicy());
                }
                return threadPool;
            }
        }
    }
    // 七个参数
    // 1. 核心线程数
    // 2. 最大线程数
    // 3. 空闲线程最大存活时间
    // 4. 时间单位
    // 5. 阻塞队列
    // 6. 创建线程工厂
    // 7. 拒绝策略
}
