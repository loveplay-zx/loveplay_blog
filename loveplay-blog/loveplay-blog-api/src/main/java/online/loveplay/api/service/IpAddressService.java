package online.loveplay.api.service;

import online.loveplay.api.dto.IpAddressDto;
import online.loveplay.api.vo.IpAddressVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2019-10-31
 */
public interface IpAddressService {

    void update(Integer id, String ip);

    /**
     * 查询
     * @param address
     * @return
     */
    List<IpAddressDto> selectList(IpAddressVo address);

    /**
     * 通过用户id查询
     * @param managerId 用户id
     * @return
     */
    List<IpAddressDto> selectIpByManagerId(Integer managerId);
}
