package online.loveplay.web.safe.config;

import online.loveplay.api.dto.ManagerDto;
import online.loveplay.api.service.ManagerService;
import online.loveplay.web.safe.entity.LoginUser;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @Author ：zx
 * @Date ：15:12 2022/5/6
 * @Description ：
 */
@Service
public class UserDetailsServiceImpl  implements UserDetailsService {

    @DubboReference
    private ManagerService managerService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        ManagerDto managerDto = managerService.findByName(s);
        if (Objects.isNull(managerDto)) {
            throw new RuntimeException("用户名和密码错误！");
        }
        LoginUser loginUser = new LoginUser();
        loginUser.managerDtoToLoginUser(managerDto);
        return loginUser;
    }
}

