package online.loveplay.api.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import online.loveplay.api.BaseTreeNode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2019-09-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CommentDto extends BaseTreeNode implements Serializable  {

    private static final long serialVersionUID = 1L;

    /**
     * 文章id
     */

    private Integer articleId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 文章id
     */

    private String articleName;

    /**
     * 评论时间
     */
    private String createTime;

    /**
     * 已登录评论人
     */

    private Integer byManagerId;


    /**
     * 已登录获取登陆人名称，未登录随意游客名称
     */
    private String  nickname;

}
