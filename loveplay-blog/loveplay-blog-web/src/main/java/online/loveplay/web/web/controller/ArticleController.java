package online.loveplay.web.web.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import online.loveplay.api.BusinessException;
import online.loveplay.api.PageBounds;
import online.loveplay.api.dto.*;
import online.loveplay.api.service.ArticleService;
import online.loveplay.api.service.CategoryService;
import online.loveplay.api.vo.ArticleVo;

import online.loveplay.web.safe.entity.ResponseResult;
import org.apache.dubbo.config.annotation.DubboReference;

import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author zx
 */
@RestController
@RequestMapping("article")
public class ArticleController extends BaseController {

    @DubboReference
    private ArticleService articleService;
    @DubboReference
    private CategoryService categoryService;

    /**
     * 搜索标题，描述，内容
     *
     * @param keywords
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @return result
     */
    @GetMapping("/search")
    public ResponseResult search(String keywords, Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        List<ArticleDto> articleList = articleService.getArticleList(keywords, currentPage, pageSize);
        if (null != articleList) {
            PageBounds<ArticleDto> pageBounds = new PageBounds(currentPage, articleList.size(), pageSize);
            pageBounds.setPageList(articleList);
            result.setData(pageBounds);
        } else {
            result.setData(null);
        }
        return result;
    }


    /**
     * 发表文章
     */
    @PostMapping("/save")
    public ResponseResult saveArticle(@RequestBody ArticleVo article) {
        ResponseResult result = new ResponseResult();
        check(article);
        articleService.updateArticle(article);
        return result;
    }

    /**
     * 修改文章
     */
    @PutMapping("/update")
    public ResponseResult updateArticle(@RequestBody ArticleVo article) {
        ResponseResult result = new ResponseResult();
        check(article);
        articleService.updateArticle(article);
        return result;
    }

    private void check(ArticleVo article) {
        ManagerDto manager = getManager();
        if (manager == null) {
            throw new BusinessException("当前用户未登录");
        } else {
            article.setManagerName(manager.getName());
        }
    }

    /**
     * 删除文章
     */
    @DeleteMapping("/delete")
    public ResponseResult deleteArticle(Integer[] ids) {
        ResponseResult result = new ResponseResult();
        articleService.deleteArticle(Arrays.asList(ids));
        return result;
    }

    /**
     * 分页查询
     */
    @GetMapping("/list")
    public ResponseResult getArticlePage(ArticleVo article, Integer pageSize, Integer currentPage) {
        ResponseResult result = new ResponseResult();
        if (pageSize != null && currentPage != null) {
            PageHelper.startPage(currentPage, pageSize);
            List<ArticleDto> articleList = articleService.getArticleByPage(article);
            PageInfo<ArticleDto> pageInfo = new PageInfo<>(articleList);
            result.setData(pageInfo);
        } else {
            List<ArticleDto> articleList = articleService.getArticleByPage(article);
            result.setData(articleList);
        }
        return result;
    }


    /**
     * 根据id查询文章具体内容
     *
     * @param id 文章id
     * @return result
     */
    @GetMapping(value = "/{id}")
    public ResponseResult searchById(@PathVariable(value = "id") int id) {
        ResponseResult result = new ResponseResult();
        ArticleDto article = articleService.searchById(id);
        result.setData(article);
        return result;
    }

    /**
     * 根据id查询文章具体内容
     *
     * @return result
     */
    @GetMapping(value = "/searchArticle")
    public ResponseResult searchByArticleIdAndManagerId(int articleId, int managerId) {
        ResponseResult result = new ResponseResult();
        ManagerDto manager = getManager();
        if (!manager.getId().equals(managerId)) {
            throw new BusinessException("当前登录人和文章博主不一致");
        }
        ArticleDto article = articleService.searchByArticleIdAndManagerId(articleId, managerId);
        result.setData(article);
        return result;
    }

    /**
     * 查询标签墙
     *
     * @return 标签内容
     */
    @GetMapping(value = "/searchTag")
    public ResponseResult searchTag() {
        ResponseResult result = new ResponseResult();
        // 对所有索引进行搜索
        List<BlogTagDto> string = articleService.searchTag();
        result.setData(string);
        return result;
    }

    /**
     * 文章归档
     */
    @GetMapping(value = "/timeLine")
    public ResponseResult searchTimeLine() {
        ResponseResult result = new ResponseResult();
        // 对所有索引进行搜索
        List<TimeLineDto> timeLine = articleService.searchTimeLine();
        result.setData(timeLine);
        return result;
    }

    /**
     * 获取全部的文章类型
     *
     * @return result
     */
    @GetMapping("getAllArticleType")
    public ResponseResult getAllArticleType() {
        ResponseResult result = new ResponseResult();
        List<CategoryDto> lists = categoryService.findAll();

        result.setData(lists);
        return result;
    }

}
