package online.loveplay.web.web.controller;

import online.loveplay.api.dto.ManagerDto;
import online.loveplay.web.safe.entity.LoginUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author ：zx
 * @Date ：14:43 2022/5/6
 * @Description ：
 */
@RestController
public class BaseController {

    public ManagerDto getManager(){

        LoginUser loginUser = (LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ManagerDto managerDto = new ManagerDto();
        managerDto.setId(loginUser.getId());
        managerDto.setName(loginUser.getName());
        managerDto.setUsername(loginUser.getUsername());
        managerDto.setPassword(loginUser.getPassword());
        managerDto.setAgiPassword(loginUser.getAgiPassword());
        managerDto.setHeadPic(loginUser.getHeadPic());
        managerDto.setCreateTime(loginUser.getCreateTime());
        managerDto.setStatus(loginUser.getStatus());
        managerDto.setType(loginUser.getType());
        managerDto.setCode(loginUser.getCode());

        return managerDto;
    }
}
