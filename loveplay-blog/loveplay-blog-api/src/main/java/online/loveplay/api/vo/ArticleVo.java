package online.loveplay.api.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zx
 */
@Data
public class ArticleVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 文章标题
     */
    private String articleName;

    /**
     * 文章封面
     */
    private String articleHeadPic;

    /**
     * 文章标签
     */
    private String articleTag;

    /**
     * 文章备注
     */
    private String articleRemark;

    /**
     * 文章阅读量
     */
    private Integer articleReadCount;

    /**
     * 文章审核状态 0：未通过；1：通过
     */
    private Integer articleState;

    /**
     * 文章内容
     */
    private String articleContent;

    /**
     * 文章作者id
     */
    private Integer managerId;

    /**
     * 作者名称
     */
    private String managerName;

    /**
     *创建时间
     */
    private String createTime;

    private Integer articleType;

    private Integer articleStarNum;

    private Integer articleConNum;

    private String enclosure;

    /**
     * 最新
     */
    private boolean latest;
    /**
     * 点赞
     */
    private boolean favorite;
    /**
     * 评论
     */
    private boolean commentMost;
    /**
     * 阅读量
     */
    private boolean recommend;

    public String[] getTags(){
        if (articleTag != null){
            String[] split = articleTag.split(",");
            return  split;
        }
        return null;
    }



}