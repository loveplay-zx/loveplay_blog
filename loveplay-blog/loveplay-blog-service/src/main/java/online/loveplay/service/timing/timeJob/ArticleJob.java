package online.loveplay.service.timing.timeJob;

import online.loveplay.api.service.SynExService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;


/**
 * 通过定时任务删除创建索引
 */
@Configuration
@Component
@EnableScheduling
public class ArticleJob implements Job {

    private Logger logger = org.slf4j.LoggerFactory.getLogger(ArticleJob.class);
    //注入需要执行任务的service
    @Resource
    private SynExService synExService;


    @Override
    public void execute(JobExecutionContext jobExecutionContext){
        logger.info("begin-自动同步文章数据到es");
        synExService.synEs();
        logger.info("end-自动同步文章数据到es");
    }

}
