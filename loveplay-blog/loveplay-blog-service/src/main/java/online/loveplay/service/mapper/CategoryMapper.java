package online.loveplay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import online.loveplay.service.bean.Category;

public interface CategoryMapper extends BaseMapper<Category> {
}
